package ru.korkmasov.tsc.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.exception.AbstractException;

public class EmptyPasswordHashException extends AbstractException {

    @NotNull
    public EmptyPasswordHashException() {
        super("Error. Password hash is empty");
    }

}
