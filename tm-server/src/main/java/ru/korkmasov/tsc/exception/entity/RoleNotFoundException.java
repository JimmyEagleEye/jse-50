package ru.korkmasov.tsc.exception.entity;

import ru.korkmasov.tsc.exception.AbstractException;

public class RoleNotFoundException extends AbstractException {

    public RoleNotFoundException() {
        super("Error! Role not found...");
    }

}
