package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.ICommandRepository;
import ru.korkmasov.tsc.api.service.ICommandService;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.util.ValidationUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;


public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        if (ValidationUtil.isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (ValidationUtil.isEmpty(arg)) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Nullable
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @NotNull
    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandArgs();
    }

    @NotNull
    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
