package ru.korkmasov.tsc.component;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.service.IPropertyService;
import ru.korkmasov.tsc.bootstrap.Bootstrap;
import ru.korkmasov.tsc.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;
    private static final int INTERVAL = 10;

    @NotNull
    private Collection<String> commands = new ArrayList<>();

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        for (@NotNull final AbstractCommand command : bootstrap.getCommandService().getArguments()) {
            commands.add(command.name());
        }
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void run() {
        @NotNull final File file = new File("./");
        for (@NotNull final File item : file.listFiles()) {
            if (!item.isFile()) continue;
            @NotNull final String fileName = item.getName();
            final boolean check = commands.contains(fileName);
            if (!check) continue;
            bootstrap.runCommand(fileName);
            item.delete();
        }
    }

}
