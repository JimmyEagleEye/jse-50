package ru.korkmasov.tsc.exception.entity;


import ru.korkmasov.tsc.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error. Task not found.");
    }

}
