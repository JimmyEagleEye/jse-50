package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.model.User;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-clear";
    }

    @Override
    public @NotNull String description() {
        return "Delete all projects";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        serviceLocator.getProjectEndpoint().clearProject(serviceLocator.getSession());
    }

}

